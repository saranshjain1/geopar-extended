from combinations.angle import Angle
from combinations.trianglefigure import TriangulatedFigure
from combinations.common import FindPossibleAngles

class GeoparOutput:

    def __init__(self, tf):
        if not isinstance(tf, TriangulatedFigure):
            raise Exception("tf is not a valid triangulated figure!")
        self.tf = tf

    def extract(self):
        """
        returns the possible angles for each point
        """
        triangles = self.tf._triangles
        mappings = list()
        for triangle in triangles:
            points, angles = triangle.points, triangle.angles
            mapping_angles = dict()
            i = 0
            j = 0
            while i < len(points) and j < len(angles):
                possible_angles = FindPossibleAngles(self.tf, angles[j]).find_possible_pairs()
                if points[i] not in mapping_angles:
                    mapping_angles[points[i]] = possible_angles
                i = i + 1
                j = j + 1
            mappings.append(mapping_angles)
        return mappings
    
    def get_formatted_output(self):
        """
        returns the output in a format geopar can understand

        1, 3, 9; alpha, beta, 180-alpha-beta
        .
        .
        .
        """
        pass
from combinations.angle import Angle
from combinations.trianglefigure import TriangulatedFigure


__author__ = 'saransh'

class FindPossibleAngles:

    def __init__(self, tf, angle):
        if not isinstance(angle, Angle):
            raise Exception("not a valid angle")
        if not isinstance(tf, TriangulatedFigure):
            raise Exception("not a valid triangulated figure")
        self.angle = angle
        self.tf = tf
    
    def get_all_angles(self):
        """
        returns all the angles in triangulated figure
        """
        triangles = self.tf._triangles
        angles = []
        for triangle in triangles:
            t_angles = triangle.angles
            angles.extend(t_angles)
        return angles
    
    def find_even_angles(self, angles):
        """
        returns even angles in trianglulated figure
        """
        return [angles[i] for i in range(0, len(angles)) if i % 2 == 0]
    
    def find_odd_angles(self, angles):
        """
        returns odd angles in triangulated figure
        """
        return [angles[i] for i in range(0, len(angles)) if i % 2 != 0]
    
    def find_position(self, angle):
        """
        find position of the angle in angles
        """
        angles = self.get_all_angles()
        position = -1
        for i in range(0, len(angles)):
            if angles[i].__eq__(angle):
                position = i
        return position

    def find_possible_pairs(self):
        """ 
        returns possible angles for the angle in triangulated figure
        """
        pos = self.find_position(self.angle)
        if pos == -1:
            raise Exception("Angle not found in any triangle")
        angles = self.get_all_angles()
        if pos % 2 == 0:
            # for an even angle possible angles are odd
            pairs = self.find_odd_angles(angles)
        else:
            # for an odd angle possible angles are even
            pairs = self.find_even_angles(angles)
        return pairs